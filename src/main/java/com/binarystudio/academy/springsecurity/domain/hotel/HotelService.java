package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class HotelService {
	private final HotelRepository hotelRepository;

	public HotelService(HotelRepository hotelRepository) {
		this.hotelRepository = hotelRepository;
	}

	public void delete(UUID hotelId) {
		var userDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		var ownerId = hotelRepository.getById(hotelId).orElseThrow(() ->
				new NoSuchElementException("Not found")).getOwnerId();
		if ((ownerId != null && ownerId.equals(userDetails.getId())) || userDetails.getAuthorities().contains(UserRole.ADMIN)) {
			System.out.println(userDetails + " " + ownerId);
			hotelRepository.delete(hotelId);
		}else{
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Access denied");
		}
	}

	public List<Hotel> getAll() {
		return hotelRepository.getHotels();
	}

	public Hotel update(Hotel hotel) {
		var userDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		var ownerId = getById(hotel.getId()).getOwnerId();
		if (!(ownerId != null && ownerId.equals(userDetails.getId())) && !userDetails.getAuthorities().contains(UserRole.ADMIN)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Access denied");
		}
		return hotelRepository.save(hotel);
	}

	public Hotel create(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public Hotel getById(UUID hotelId) {
		return hotelRepository.getById(hotelId).orElseThrow();
	}
}
